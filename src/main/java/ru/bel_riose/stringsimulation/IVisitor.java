/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation;

import ru.bel_riose.stringsimulation.model.FlexibleStringFast;
import ru.bel_riose.stringsimulation.model.FlexibleStringPoints;
import ru.bel_riose.stringsimulation.model.Sleeve;

/**
 *
 * @author Ivan
 */
public interface IVisitor {
    public void visitSleeve(Sleeve sleeve);
    public void visitFlexibleStringPoints(FlexibleStringPoints fString);
    public void visitFlexibleStringFast(FlexibleStringFast fString);
    
}
