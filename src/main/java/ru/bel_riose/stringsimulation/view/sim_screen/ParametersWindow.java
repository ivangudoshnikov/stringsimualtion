/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation.view.sim_screen;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;
import ru.bel_riose.libgdx_2d_skeleton.gui.Message;

/**
 *
 * @author Ivan Gudoshnikov
 */
public class ParametersWindow extends Window {

    private final static String PARAMETERS_TEXT = "Параметры",
            ERROR_TEXT = "Ошибка", CANTUPDATE_TEXT = "Не могу обновить параметр",
            T_TEXT="t:",DT_TEXT = "Шаг:",UPDATESPERFRAME_TEXT = "Шагов за кадр:", SLEEVEPOS_TEXT = "Позиция втулки:", SLEEVEHEIGHT_TEXT = "Высота втулки:",
            SLEEVEBOUNCE_TEXT = "Упругость втулки:", RESISTANCE_TEXT = "Сопротивление среды:";
    private TextField txtDt, txtUpdatesPerFrame, txtSleeveLeft, txtSleeveRight, txtSleeveH, txtSleeveK, txtResistance;
    private Label lblT;
    private SimScreenGUI gui;

    public ParametersWindow(final SimScreenGUI gui) {
        super(PARAMETERS_TEXT, gui.getSkin());
        this.gui = gui;
        
        add(new Label(T_TEXT, gui.getSkin()));
        lblT=new Label(String.format("%.3f",gui.getScreen().getApplication().getSim().getT()), gui.getSkin());
        add(lblT);
        row();
        
        add(new Label(DT_TEXT, gui.getSkin()));
        txtDt = new TextField(String.valueOf(gui.getScreen().getApplication().getSim().getDt()), gui.getSkin());
        add(txtDt);
        txtDt.addListener(new FocusListener() {
            @Override
            public void keyboardFocusChanged(FocusListener.FocusEvent event, Actor actor, boolean focused) {
                if (!focused) {
                    try {
                        gui.getScreen().getApplication().getSim().setDt(Double.valueOf(txtDt.getText()));
                    } catch (Exception ex) {
                        new Message(ERROR_TEXT, CANTUPDATE_TEXT, gui.getStage(), gui.getSkin());
                        gui.update();
                    }
                }
            }
        });
        row();

        add(new Label(UPDATESPERFRAME_TEXT, gui.getSkin()));
        txtUpdatesPerFrame = new TextField(String.valueOf(gui.getScreen().getApplication().getUpdatesPerFrame()), gui.getSkin());
        add(txtUpdatesPerFrame);
        txtUpdatesPerFrame.addListener(new FocusListener() {
            @Override
            public void keyboardFocusChanged(FocusListener.FocusEvent event, Actor actor, boolean focused) {
                if (!focused) {
                    try {
                        gui.getScreen().getApplication().setUpdatesPerFrame(Integer.valueOf(txtUpdatesPerFrame.getText()));

                    } catch (Exception ex) {
                        new Message(ERROR_TEXT, CANTUPDATE_TEXT, gui.getStage(), gui.getSkin());
                        gui.update();
                    }
                }
            }
        });
        row();

        add(new Label(SLEEVEPOS_TEXT, gui.getSkin()));
        txtSleeveLeft = new TextField(String.valueOf(gui.getScreen().getApplication().getSim().getSleeve().getLeft()), gui.getSkin());
        add(txtSleeveLeft);
        txtSleeveLeft.addListener(new FocusListener() {
            @Override
            public void keyboardFocusChanged(FocusListener.FocusEvent event, Actor actor, boolean focused) {
                if (!focused) {
                    try {
                        gui.getScreen().getApplication().getSim().getSleeve().setLeft(Double.valueOf(txtSleeveLeft.getText()));

                    } catch (Exception ex) {
                        new Message(ERROR_TEXT, CANTUPDATE_TEXT, gui.getStage(), gui.getSkin());
                        gui.update();
                    }
                }
            }
        });
        txtSleeveRight = new TextField(String.valueOf(gui.getScreen().getApplication().getSim().getSleeve().getRight()), gui.getSkin());
        txtSleeveRight.addListener(new FocusListener() {
            @Override
            public void keyboardFocusChanged(FocusListener.FocusEvent event, Actor actor, boolean focused) {
                if (!focused) {
                    try {
                        gui.getScreen().getApplication().getSim().getSleeve().setRight(Double.valueOf(txtSleeveRight.getText()));

                    } catch (Exception ex) {
                        new Message(ERROR_TEXT, CANTUPDATE_TEXT, gui.getStage(), gui.getSkin());
                        gui.update();
                    }
                }
            }
        });
        add(txtSleeveRight);
        row();

        add(new Label(SLEEVEHEIGHT_TEXT, gui.getSkin()));
        txtSleeveH = new TextField(String.valueOf(gui.getScreen().getApplication().getSim().getSleeve().getHeight()), gui.getSkin());
        add(txtSleeveH);
        txtSleeveH.addListener(new FocusListener() {
            @Override
            public void keyboardFocusChanged(FocusListener.FocusEvent event, Actor actor, boolean focused) {
                if (!focused) {
                    try {
                        gui.getScreen().getApplication().getSim().getSleeve().setHeight(Double.valueOf(txtSleeveH.getText()));
                    } catch (Exception ex) {
                        new Message(ERROR_TEXT, CANTUPDATE_TEXT, gui.getStage(), gui.getSkin());
                        gui.update();
                    }
                }
            }
        });
        row();

        add(new Label(SLEEVEBOUNCE_TEXT, gui.getSkin()));
        txtSleeveK = new TextField(String.valueOf(gui.getScreen().getApplication().getSim().getSleeve().getK()), gui.getSkin());
        add(txtSleeveK);
        txtSleeveK.addListener(new FocusListener() {
            @Override
            public void keyboardFocusChanged(FocusListener.FocusEvent event, Actor actor, boolean focused) {
                if (!focused) {
                    try {
                        gui.getScreen().getApplication().getSim().getSleeve().setK(Double.valueOf(txtSleeveK.getText()));

                    } catch (Exception ex) {
                        new Message(ERROR_TEXT, CANTUPDATE_TEXT, gui.getStage(), gui.getSkin());
                        gui.update();
                    }
                }
            }
        });
        row();

        add(new Label(RESISTANCE_TEXT, gui.getSkin()));
        txtResistance = new TextField(String.valueOf(gui.getScreen().getApplication().getSim().getFlexibleString().getEnviromentResistance()), gui.getSkin());
        add(txtResistance);
        txtResistance.addListener(new FocusListener() {
            @Override
            public void keyboardFocusChanged(FocusListener.FocusEvent event, Actor actor, boolean focused) {
                if (!focused) {
                    try {
                        gui.getScreen().getApplication().getSim().getFlexibleString().setEnviromentResistance(Double.valueOf(txtResistance.getText()));

                    } catch (Exception ex) {
                        new Message(ERROR_TEXT, CANTUPDATE_TEXT, gui.getStage(), gui.getSkin());
                        gui.update();
                    }
                }
            }
        });
        this.pack();
        gui.getStage().addActor(this);
    }

    public void update() {
        lblT.setText(String.format("%.3f",gui.getScreen().getApplication().getSim().getT()));
        txtDt.setText(String.valueOf(gui.getScreen().getApplication().getSim().getDt()));
        txtUpdatesPerFrame.setText(String.valueOf(gui.getScreen().getApplication().getUpdatesPerFrame()));
        txtSleeveLeft.setText(String.valueOf(gui.getScreen().getApplication().getSim().getSleeve().getLeft()));
        txtSleeveRight.setText(String.valueOf(gui.getScreen().getApplication().getSim().getSleeve().getRight()));
        txtSleeveH.setText(String.valueOf(gui.getScreen().getApplication().getSim().getSleeve().getHeight()));
        txtSleeveK.setText(String.valueOf(gui.getScreen().getApplication().getSim().getSleeve().getK()));
        txtResistance.setText(String.valueOf(gui.getScreen().getApplication().getSim().getFlexibleString().getEnviromentResistance()));
    }
}
