/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation.view.sim_screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import ru.bel_riose.libgdx_2d_skeleton.InputHandler;

/**
 *
 * @author Ivan
 */
public class SelectRectInputhandler extends InputHandler<SimScreen> {

    public SelectRectInputhandler(SimScreen screen) {
        super(screen);
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.ESCAPE:
                Gdx.app.exit();
                break;
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return true;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(button==Input.Buttons.RIGHT){
            screen.setDeafultInputProcessor();
            screen.selectedRegionFrame.visible=false;
            screen.selectedRegionFrame.restoreFromBackup();
        }else if(button==Input.Buttons.LEFT){
            if(!screen.selectedRegionFrame.point1Fixed){
                screen.selectedRegionFrame.setPoint1(screenX, screenY);
                screen.selectedRegionFrame.setPoint2(screenX, screenY);                
                screen.selectedRegionFrame.visible=true;    
                screen.selectedRegionFrame.point1Fixed=true;
            }else{
                screen.selectedRegionFrame.setPoint2(screenX, screenY);
                screen.selectedRegionFrame.visible=false;    
                screen.selectedRegionFrame.point1Fixed=false;
                screen.setDeafultInputProcessor();
                screen.getApplication().getShotmaker().
                        setScreenshotRect(screen.selectedRegionFrame.
                                getRect(screen.getCamera2d().getWidth()/2,
                                        screen.getCamera2d().getHeight()/2));
            }
        
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if(screen.selectedRegionFrame.point1Fixed){
            screen.selectedRegionFrame.setPoint2(screenX, screenY);
        }
        return true;
    }

    @Override
    public boolean scrolled(int amount) {
        return true;
    }

}
