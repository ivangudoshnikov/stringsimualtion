/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation.view.sim_screen;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import ru.bel_riose.libgdx_2d_skeleton.gui.Message;

/**
 *
 * @author Ivan Gudoshnikov
 */
public class FileWindow  extends Window{
    private static final String TITLE_TEXT="Общее",RESTART_TEXT="Рестарт", ABOUT_TEXT="О программе", 
            EXIT_TEXT="Выход",
            AUTHOR_TEXT="(С) Иван Гудошников, 2012";
    private  SimScreenGUI gui;
    
    private TextButton btnRestart, btnAbout, btnExit;
    public FileWindow(final SimScreenGUI gui) {
        super(TITLE_TEXT, gui.getSkin());
        this.gui=gui;
        
        btnRestart= new TextButton(RESTART_TEXT, gui.getSkin());
        btnRestart.addListener(new ClickListener(Input.Buttons.LEFT){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                gui.getScreen().getApplication().restart();
                gui.update();
            }
        });
        add(btnRestart);
        
        btnAbout= new TextButton(ABOUT_TEXT, gui.getSkin());
        btnAbout.addListener(new ClickListener(Input.Buttons.LEFT){

            @Override
            public void clicked(InputEvent event, float x, float y) {
                new Message(ABOUT_TEXT, gui.getScreen().getApplication().TITLE+" "+gui.getScreen().getApplication().VERSION+"\r\n"+AUTHOR_TEXT, gui.getStage(), gui.getSkin());
            }
        });
        add(btnAbout);
        
        btnExit= new TextButton(EXIT_TEXT, gui.getSkin());
        btnExit.addListener(new ClickListener(Input.Buttons.LEFT){

            @Override
            public void clicked(InputEvent event, float x, float y) {
                gui.getScreen().getApplication().dispose();
                System.exit(0);
            }
        });
        add(btnExit);
        pack();
        gui.getStage().addActor(this);
        setPosition(0, gui.getStage().getHeight());
        
    }
    
    
}
