/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation.view.sim_screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import ru.bel_riose.libgdx_2d_skeleton.MapCamera2D;
import ru.bel_riose.libgdx_2d_skeleton.Screen;
import ru.bel_riose.stringsimulation.MyApp;

/**
 *
 * @author Ivan
 */
public class SimScreen extends Screen<MyApp, MapCamera2D, SimScreenInputHandler, SimScreenGUI> {

    final SelectedRegionFrame selectedRegionFrame;

    public SimScreen(MyApp application) {
        super(application);
        camera2d = new MapCamera2D(0, 0, 1, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        defaultInputHandler = new SimScreenInputHandler(this);
        gui = new SimScreenGUI(this);

        selectedRegionFrame = new SelectedRegionFrame(this,0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        shapeRenderer.setProjectionMatrix(camera2d.getViewProjectionMatrix());
        application.getSim().draw(shapeRenderer);
        if (application.isSimRunning()) {
            gui.update();
        }
        gui.actNdraw();

        selectedRegionFrame.draw(shapeRenderer);
    }
}
