/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation.view.sim_screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import ru.bel_riose.libgdx_2d_skeleton.MapCamera2D;
import ru.bel_riose.libgdx_2d_skeleton.MapInputHandler;
import ru.bel_riose.stringsimulation.Shotmaker;

/**
 *
 * @author Ivan
 */
public class SimScreenInputHandler extends MapInputHandler<SimScreen> {

    public SimScreenInputHandler(SimScreen screen) {
        super(screen);
    }

    @Override
    protected MapCamera2D getCamera() {
        return screen.getCamera2d();
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.BACKSPACE:
                screen.getCamera2d().restoreDefaults();
                break;
            case Input.Keys.ESCAPE:
                Gdx.app.exit();
                break;
            case Input.Keys.SPACE:
                screen.getApplication().setSimRunning(!screen.getApplication().isSimRunning());
                break;
            case Input.Keys.S:
                screen.getApplication().getShotmaker().singleShot();
                break;
            case Input.Keys.A:
                Shotmaker shotmaker = screen.getApplication().getShotmaker();
                if (shotmaker.isFilming()) {
                    shotmaker.stopFilming();
                } else {
                    shotmaker.startFilming(screen.getApplication().getSim().getT());
                }
                screen.getGui().update();
                break;
        }
        return true;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        screen.getGui().clearFoci();
        return super.touchDown(screenX, screenY, pointer, button);
    }

}
