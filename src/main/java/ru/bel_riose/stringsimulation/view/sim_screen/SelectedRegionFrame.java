/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.bel_riose.stringsimulation.view.sim_screen;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.stringsimulation.Shotmaker;

/**
 *
 * @author Ivan
 */
class SelectedRegionFrame {

        public SelectedRegionFrame(SimScreen screen,int x1, int y1, int x2, int y2) {
            this.screen=screen;
            p1 = backupP1 = screen.getCamera2d().mouseToScr(x1, y1);
            p2 = backupP2 = screen.getCamera2d().mouseToScr(x2, y2);
        }
        
        private final SimScreen screen;
            
        private Vector p1, p2, backupP1, backupP2;

        boolean visible;
        boolean point1Fixed;

        void setPoint1(int x, int y) {
            p1 = screen.getCamera2d().mouseToScr(x, y);
        }

        void setPoint2(int x, int y) {
            p2 = screen.getCamera2d().mouseToScr(x, y);
        }

        void makeBackup(){
            backupP1=p1;
            backupP2=p2;
        }
        
        void restoreFromBackup(){
            p1=backupP1;
            p2=backupP2;
        }
        
        Shotmaker.ScreenRectangleRegion getRect() {
            return getRect(0, 0);
        }
        
        Shotmaker.ScreenRectangleRegion getRect(double dx,double dy) {
            double x = Math.min(p1.x, p2.x)+dx,
                    w = Math.abs(p1.x - p2.x),
                    y = Math.min(p1.y, p2.y)+dy,
                    h = Math.abs(p1.y - p2.y);
            return new Shotmaker.ScreenRectangleRegion(x, y, w, h);            
        }

        void draw(ShapeRenderer shapeRenderer) {
            if (visible) {
                shapeRenderer.setColor(0, 0, 0.9f, 1);
                Shotmaker.ScreenRectangleRegion rect = getRect();
                Vector v = screen.getCamera2d().scrToMath(rect.x, rect.y);
                shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
                shapeRenderer.rect((float) v.x, (float) v.y, (float) screen.getCamera2d().rescaleScrToMath(rect.w), (float) screen.getCamera2d().rescaleScrToMath(rect.h));
                shapeRenderer.end();
            }
        }   
}
