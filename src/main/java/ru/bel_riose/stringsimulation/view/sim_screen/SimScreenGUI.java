/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.bel_riose.stringsimulation.view.sim_screen;

import ru.bel_riose.libgdx_2d_skeleton.GraphicalUserInterface;

/**
 *
 * @author Ivan
 */
public class SimScreenGUI extends GraphicalUserInterface<SimScreen>{
    private ParametersWindow parametersWindow;
    private FileWindow fileWindow;
    private ShotmakerWindow shotmakerWindow;

    public SimScreenGUI(SimScreen screen) {
        super(screen);
        fileWindow= new FileWindow(this);
        parametersWindow= new ParametersWindow(this);
        shotmakerWindow=new ShotmakerWindow(this);
    }
    
    public void clearFoci(){
        stage.setKeyboardFocus(null);
        stage.setScrollFocus(null);
    }
    
    @Override
    public void update() {
        parametersWindow.update(); 
        shotmakerWindow.update();
    }

    @Override
    public void resize(float w, float h) {
        super.resize(w, h); 
        fileWindow.setPosition(0, stage.getHeight());
    }
    
    
}
