/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation.view.sim_screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;
import ru.bel_riose.stringsimulation.Shotmaker;

/**
 *
 * @author Ivan
 */
public class ShotmakerWindow extends Window {

    private static final String TITLE_TEXT = "Съемка", SELECT_RECT_TEXT = "Выбрать область съемки",
            SINGLE_SHOT_TEXT = "Одиночный скриншотs(S)", LAST_SHOT_T_TEXT="Последний кадр T=",
            SHOT_DT_TEXT="Интервал съемки: ",FILMING_ON_TEXT = "Съемка идет",
            FILMING_OFF_TEXT = "Съемка остановлена", FILMING_TEXT = "Старт/стоп съемки(A)";

    private final SimScreenGUI gui;
    private final Label lblFilming,lblLastShotT;
    private final Shotmaker shotmaker;

    public ShotmakerWindow(final SimScreenGUI gui) {
        super(TITLE_TEXT, gui.getSkin());
        this.gui = gui;
        shotmaker = gui.getScreen().getApplication().getShotmaker();
        TextButton btnSelectScreenshotRect = new TextButton(SELECT_RECT_TEXT, gui.getSkin());
        btnSelectScreenshotRect.addListener(new ClickListener(Input.Buttons.LEFT) {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                gui.getScreen().selectedRegionFrame.visible = false;
                gui.getScreen().selectedRegionFrame.point1Fixed = false;
                gui.getScreen().selectedRegionFrame.makeBackup();
                Gdx.input.setInputProcessor(new SelectRectInputhandler(gui.getScreen()));
            }
        });
        add(btnSelectScreenshotRect);
        row();
        TextButton btnSingleShot = new TextButton(SINGLE_SHOT_TEXT, gui.getSkin());
        btnSingleShot.addListener(new ClickListener(Input.Buttons.LEFT) {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                shotmaker.singleShot();
            }
        });
        add(btnSingleShot);
        row();
        lblFilming = new Label(shotmaker.isFilming() ? FILMING_ON_TEXT : FILMING_OFF_TEXT, gui.getSkin());
        add(lblFilming);
        row();        
        lblLastShotT= new Label(LAST_SHOT_T_TEXT+String.format("%.3f",shotmaker.getLastShotT()),gui.getSkin());
        add(lblLastShotT);
        row();
        Table tblShotDT= new Table(gui.getSkin());
        tblShotDT.add(new Label(SHOT_DT_TEXT, gui.getSkin()));
        final TextField txtShotDT = new TextField(String.valueOf(shotmaker.getShotDT()), gui.getSkin());
        txtShotDT.addListener(new FocusListener() {

            @Override
            public void keyboardFocusChanged(FocusListener.FocusEvent event, Actor actor, boolean focused) {
                if (!focused) {
                    double shotDT = shotmaker.getShotDT();
                    try {
                        shotDT = Double.parseDouble(txtShotDT.getText());
                    } catch (NumberFormatException e) {
                        txtShotDT.setText(String.valueOf(shotDT));
                    }
                    shotmaker.setShotDT(shotDT);
                }
            }
        }
        );
        tblShotDT.add(txtShotDT);
        add(tblShotDT);
        row();
        TextButton btnFilming = new TextButton(FILMING_TEXT, gui.getSkin());
        btnFilming.addListener(new ClickListener(Input.Buttons.LEFT) {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (shotmaker.isFilming()) {
                    shotmaker.stopFilming();
                } else {
                    shotmaker.startFilming(gui.getScreen().getApplication().getSim().getT());
                }
                update();
            }
        });
        add(btnFilming);

        pack();
        gui.getStage().addActor(this);
        setPosition(gui.getStage().getWidth(), gui.getStage().getHeight());
    }

    void update() {
        lblFilming.setText(shotmaker.isFilming() ? FILMING_ON_TEXT : FILMING_OFF_TEXT);
        lblLastShotT.setText(LAST_SHOT_T_TEXT+String.format("%.3f",shotmaker.getLastShotT()));
    }
;
}
