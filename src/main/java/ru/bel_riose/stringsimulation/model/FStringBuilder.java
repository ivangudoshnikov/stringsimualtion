/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation.model;

import java.util.LinkedList;

/**
 *
 *@author Ivan Gudoshnikov
 */
public abstract class FStringBuilder {
    public abstract LinkedList<Double> getInitialHeights();
    public abstract LinkedList<Double> getInitialSpeeds();    
}
