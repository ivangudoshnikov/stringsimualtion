/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation.model;

/**
 *
 * @author Ivan
 */
public class SinSleeve extends Sleeve {
    private double phase, amplitude, frequency;
    
    /**
     * 
     * @param k - коэффициент упругости столкновения
     * @param h - половина расстояния от низа до верха втулки("радиус")
     * @param left - координата левого конца
     * @param right - координата правого конца
     * @param phase - начальная фаза колебаний
     * @param amplitude - амплитуда колебаний
     * @param frequency - частота колебаний
     */
    public SinSleeve(double k, double h, double left, double right,double phase, double amplitude, double frequency) {
        super(k, h, left, right);
        this.phase=phase;
        this.amplitude=amplitude;
        this.frequency=frequency;
        y=amplitude*Math.sin(phase);        
    }

    public double getPhase() {
        return phase;
    }

    public void setPhase(double phase) {
        this.phase = phase;
    }

    public double getAmplitude() {
        return amplitude;
    }

    public void setAmplitude(double amplitude) {
        this.amplitude = amplitude;
    }

    public double getFrequency() {
        return frequency;
    }

    public void setFrequency(double frequency) {
        this.frequency = frequency;
    }
    
    

    @Override
    public void update(double t, double dt) {
        double prevy=y;
        y=amplitude*Math.sin((t+dt)*frequency+phase);
        vy=(y-prevy)/dt;        
    }
    
}
