/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation.model;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import java.util.Iterator;
import java.util.LinkedList;
import ru.bel_riose.geometry.Vector;
import ru.bel_riose.numericalmethods.kinematics2d.IGetAccelerationCallback;
import ru.bel_riose.numericalmethods.kinematics2d.PhysicsSolver;
import ru.bel_riose.stringsimulation.IVisitor;

/**
 *
 * @author Ivan Gudoshnikov
 */
public class FlexibleStringPoints extends FlexibleString implements IGetAccelerationCallback {

    protected LinkedList<Node> elements;
    private PhysicsSolver physicsSolver;

    public FlexibleStringPoints(LinkedList<Double> initialHeights, LinkedList<Double> initialSpeeds, double left, double right, BoundaryCondition leftCondition, BoundaryCondition rightCondition, double enviromentResistance, PhysicsSolver physicsSolver, Sleeve sleeve) {
        super(initialHeights, initialSpeeds, left, right, leftCondition, rightCondition, enviromentResistance, sleeve);
        this.physicsSolver = physicsSolver;

        elements = new LinkedList<>();
        Iterator<Double> speedsIter = initialSpeeds.iterator();
        double x = this.left;
        for (Double y : initialHeights) {
            elements.add(new Node(new Vector(x, y), new Vector(0, speedsIter.next())));
            x += gap;
        }
    }

    @Override
    public void update(double t, double dt) {
        if (leftCondition==BoundaryCondition.DIRICHLET_ZERO){
            elements.getFirst().setPos(new Vector(0, 0));
            elements.getFirst().setSpeed(new Vector(0, 0));
        }
        if (rightCondition==BoundaryCondition.DIRICHLET_ZERO){
            elements.getLast().setPos(new Vector(0, 0));
            elements.getLast().setSpeed(new Vector(0, 0));
        }
        physicsSolver.update(dt, elements, this);
        if (sleeve != null) {
            applySleeve();
        }
    }

    @Override
    public Iterable<Vector> getAcceleration(double t, Iterable<Vector> pos, Iterable<Vector> speed) {
        LinkedList<Vector> accel = new LinkedList<>();
        Iterator<Vector> posIter = pos.iterator();
        Iterator<Vector> speedIter = speed.iterator();
        Vector prevprevX = posIter.next();
        Vector prevprevV = speedIter.next();
        Vector prevX = posIter.next();
        //TODO: test!
        switch (leftCondition) {
            case DIRICHLET_ZERO:
                accel.add(new Vector(0, 0));
                break;
            case NEUMANN_ZERO:
                accel.add(new Vector(0, (prevX.y - prevprevX.y) / (gap * gap) - enviromentResistance * prevprevV.y));
                break;
        }

        while (posIter.hasNext()) {
            Vector current = posIter.next(); //на 2 впереди
            Vector prevV = speedIter.next(); //на 1 впереди
            accel.add(new Vector(0, (prevprevX.y + current.y - 2 * prevX.y) / (gap * gap) - enviromentResistance * prevV.y));
            prevprevX = prevX;
            prevX = current;
        }
        switch (rightCondition) {
            case DIRICHLET_ZERO:
                accel.add(new Vector(0, 0));
                break;
            case NEUMANN_ZERO:
                accel.add(new Vector(0, (prevprevX.y - prevX.y) / (gap * gap) - enviromentResistance * speedIter.next().y));
                break;
        }

        return accel;
    }

    @Override
    public void draw(ShapeRenderer shapeRenderer) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(0, 0, 0, 1);

        Iterator<Node> nodesIter = elements.iterator();
        Vector prev = nodesIter.next().getPos();
        while (nodesIter.hasNext()) {
            Vector current = nodesIter.next().getPos();
            shapeRenderer.line((float) prev.x, (float) prev.y, (float) current.x, (float) current.y);
            prev = current;
        }
        shapeRenderer.end();
    }

    @Override
    protected void applySleeve() {
        for (Node element : elements) {
            Vector pos = element.getPos(), speed = element.getSpeed();
            if ((pos.x >= sleeve.getLeft()) && (pos.x <= sleeve.getRight()) && (Math.abs(pos.y) >= sleeve.getHeight())) {
                element.setPos(new Vector(pos.x, sleeve.getHeight() * Math.signum(pos.y)));
                element.setSpeed(new Vector(speed.x, -sleeve.getK() * speed.y));
            }
        }
        throw new UnsupportedOperationException();
    }

    @Override
    public void acceptVisitor(IVisitor visitor) {
        visitor.visitFlexibleStringPoints(this);
    }
}
