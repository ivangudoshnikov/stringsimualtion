/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation.model;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import java.util.LinkedList;
import ru.bel_riose.numericalmethods.ode.RungeKutta4;

/**
 * Основной класс, содержит объекты сцены
 *
 * @author Ivan Gudoshnikov
 */
public class Simulation {

    private FlexibleString flexibleString;
    private Sleeve sleeve;
    private double t, dt;
    //профили - разные начальные положения струны
    //неподвижная прямая
    private static final FStringBuilder profile0 = new FStringBuilder() {
        @Override
        public LinkedList<Double> getInitialHeights() {
            LinkedList<Double> result = new LinkedList<>();
            result.add(0d);
            for (int i = 0; i < 4000; i++) {
                result.add(0d);
            }
            result.add(0d);
            return result;
        }

        @Override
        public LinkedList<Double> getInitialSpeeds() {
            LinkedList<Double> result = new LinkedList<>();
            result.add(0d);
            for (int i = -2000; i < 2000; i++) {
                result.add(0d);
            }
            result.add(0d);
            return result;
        }
    };
    //прямая со скоростями синусоидальной стоячей волны
    private static final FStringBuilder profile1 = new FStringBuilder() {
        @Override
        public LinkedList<Double> getInitialHeights() {
            LinkedList<Double> result = new LinkedList<>();
            result.add(0d);
            for (int i = 0; i < 4000; i++) {
                result.add(0d);
            }
            result.add(0d);
            return result;
        }

        @Override
        public LinkedList<Double> getInitialSpeeds() {
            LinkedList<Double> result = new LinkedList<>();
            result.add(0d);
            for (int i = -2000; i < 2000; i++) {
                result.add(Math.sin(16 * Math.PI * i / 2000d));
            }
            result.add(0d);
            return result;
        }
    };
    //оттянутый вниз по эллипсу правый бок 
    private static final FStringBuilder profile2 = new FStringBuilder() {
        @Override
        public LinkedList<Double> getInitialHeights() {
            LinkedList<Double> result = new LinkedList<>();
            result.add(0d);
            for (int i = -1000; i < 0; i++) {
                result.add(20 * Math.sin(Math.PI * i / 1000d));
            }
            for (int i = 0; i < 3000; i++) {
                result.add(0d);
            }
            result.add(0d);
            return result;
        }

        @Override
        public LinkedList<Double> getInitialSpeeds() {
            LinkedList<Double> result = new LinkedList<>();
            result.add(0d);
            for (int i = -2000; i < 2000; i++) {
                result.add(0d);
            }
            result.add(0d);
            return result;
        }
    };
    //пик
    private static final FStringBuilder profile3 = new FStringBuilder() {
        @Override
        public LinkedList<Double> getInitialHeights() {
            LinkedList<Double> result = new LinkedList<>();
            result.add(0d);
            result.add(100d);
            for (int i = 0; i < 3999; i++) {
                result.add(0d);
            }
            result.add(0d);
            return result;
        }

        @Override
        public LinkedList<Double> getInitialSpeeds() {
            LinkedList<Double> result = new LinkedList<>();
            result.add(0d);
            for (int i = -2000; i < 2000; i++) {
                result.add(0d);
            }
            result.add(0d);
            return result;
        }
    };
    //прямоугольный сигнал
    private static final FStringBuilder profile4 = new FStringBuilder() {
        @Override
        public LinkedList<Double> getInitialHeights() {
            LinkedList<Double> result = new LinkedList<>();
            result.add(0d);
            for (int i = 0; i < 250; i++) {
                result.add(0d);
            }

            for (int i = 0; i < 250; i++) {
                result.add(i * 2d / 25d);
            }

            for (int i = 0; i < 250; i++) {
                result.add(20d);
            }
            for (int i = 0; i < 250; i++) {
                result.add(20 - i * 2d / 25d);
            }

            for (int i = 0; i < 3000; i++) {
                result.add(0d);
            }
            result.add(0d);
            return result;
        }

        @Override
        public LinkedList<Double> getInitialSpeeds() {
            LinkedList<Double> result = new LinkedList<>();
            result.add(0d);
            for (int i = -2000; i < 2000; i++) {
                result.add(0d);
            }
            result.add(0d);
            return result;
        }
    };
    
    //прямоугольный сигнал
    private static final FStringBuilder profile5 = new FStringBuilder() {
        @Override
        public LinkedList<Double> getInitialHeights() {
            LinkedList<Double> result = new LinkedList<>();
            result.add(0d);
            for (int i = -1000; i < 0; i++) {
                result.add(0d);
            }
            for (int i = 0; i < 2000; i++) {
                result.add(20d);
            }
            for (int i = 2000; i < 3000; i++) {
                result.add(0d);
            }
            
            result.add(0d);
            return result;
        }

        @Override
        public LinkedList<Double> getInitialSpeeds() {
            LinkedList<Double> result = new LinkedList<>();
            result.add(0d);
            for (int i = -2000; i < 2000; i++) {
                result.add(0d);
            }
            result.add(0d);
            return result;
        }
    };

    public Simulation() {
        t = 0;
        //sleeve = new Sleeve(0, 1, -2.5, 2.5);

        //Втулка на конце:
        sleeve = new SinSleeve(0, 1, 99.975, 101, 0, 4*0, 2 * Math.PI / 200f);
        //sleeve = new SinSleeve(0, 1, -1, 1, 0, 2 * 0, 2 * Math.PI / 200f);

        flexibleString = new FlexibleStringFast(profile5.getInitialHeights(), profile5.getInitialSpeeds(),
                -100, 100, FlexibleString.BoundaryCondition.DIRICHLET_ZERO, FlexibleString.BoundaryCondition.DIRICHLET_ZERO,
                0.0, new RungeKutta4(), sleeve);
        //flexibleString = new FlexibleStringPoints(getInitialHeights(), getInitialSpeed(), -100, 100, new ODEWrapSolver(new RungeKutta4()),sleeve);

        dt = 0.05;
    }

    public void update() {
        sleeve.update(t, dt);
        flexibleString.update(t, dt);
        t += dt;

    }

    public void draw(ShapeRenderer shapeRenderer) {
        flexibleString.draw(shapeRenderer);
        sleeve.draw(shapeRenderer);
    }

    public FlexibleString getFlexibleString() {
        return flexibleString;
    }

    public Sleeve getSleeve() {
        return sleeve;
    }

    public double getDt() {
        return dt;
    }

    public void setDt(double dt) {
        this.dt = dt;
    }

    public double getT() {
        return t;
    }
}
