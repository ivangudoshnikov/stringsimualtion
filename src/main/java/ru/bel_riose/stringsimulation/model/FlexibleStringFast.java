/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation.model;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import java.util.Iterator;
import java.util.LinkedList;
import ru.bel_riose.numericalmethods.ode.IGetDerivativeCallback;
import ru.bel_riose.numericalmethods.ode.ODEMethod;
import ru.bel_riose.stringsimulation.IVisitor;

/**
 *
 * @author Ivan Gudoshnikov
 */
public class FlexibleStringFast extends FlexibleString implements IGetDerivativeCallback {

    private ODEMethod method;
    private int length;
    private Iterable<Double> chain;

    public FlexibleStringFast(LinkedList<Double> initialHeights, LinkedList<Double> initialSpeeds, double left, double right, BoundaryCondition leftCondition, BoundaryCondition rightCondition, double enviromentResistance, ODEMethod method, Sleeve sleeve) {
        super(initialHeights, initialSpeeds, left, right, leftCondition, rightCondition, enviromentResistance, sleeve);
        length = initialHeights.size();
        this.method = method;
        LinkedList<Double> chain0 = new LinkedList<>();
        Iterator<Double> speedIter = initialSpeeds.iterator();
        for (Double y : initialHeights) {
            chain0.add(y);
            chain0.add(speedIter.next());
        }

        if (leftCondition == BoundaryCondition.DIRICHLET_ZERO) {
            //координата и скорость первой точки - нули
            chain0.set(0, 0d);
            chain0.set(1, 0d);
        }

        if (rightCondition == BoundaryCondition.DIRICHLET_ZERO) {
            //координата и скорость последней точки - нули
            chain0.set(chain0.size() - 2, 0d);
            chain0.set(chain0.size() - 1, 0d);
        }

        chain = chain0;
    }

    @Override
    public void update(double t, double dt) {
        chain = method.integrate(this, 0, chain, dt);
        if (sleeve != null) {
            applySleeve();
        }
    }

    @Override
    public void draw(ShapeRenderer shapeRenderer) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(0, 0, 0, 1);
        Iterator<Double> chainIter = chain.iterator();
        double prev = chainIter.next();
        chainIter.next();//скорость первой точки
        double x = left;
        while (chainIter.hasNext()) {
            double current = chainIter.next();
            chainIter.next();
            shapeRenderer.line((float) x, (float) prev, (float) (x + gap), (float) current);
            prev = current;
            x += gap;
        }
        shapeRenderer.end();
    }

    @Override
    public Iterable<Double> getDerivative(double t, Iterable<Double> x) {
        LinkedList<Double> result = new LinkedList<>();
        Iterator<Double> xIter = x.iterator();
        double prevprevX = xIter.next(), //читаем координату первой точки
                firstV = xIter.next();
        
        double prevX = xIter.next();
        switch (leftCondition) {
            case DIRICHLET_ZERO:
                result.add(0d);//скорость первой точки
                result.add(0d);//ускорение первой точки
                break;
            case NEUMANN_ZERO:
                result.add(firstV); //пишем как производную координаты скорость первой точки
                result.add((prevX - prevprevX) / (gap * gap) - enviromentResistance * firstV); //пишем ускорение первой точки
                break;
        }

        for (int i = 2; i < length; i++) {
            double prevV = xIter.next();
            result.add(prevV); //читаем и пишем как производную координаты скорость prev
            double currentX = xIter.next(); //читаем координату
            result.add((prevprevX + currentX - 2 * prevX) / (gap * gap) - enviromentResistance * prevV); //пишем ускорение prev
            prevprevX = prevX;
            prevX = currentX;
        }
        switch(rightCondition){
            case DIRICHLET_ZERO:
                result.add(0d);//скорость последней точки
                result.add(0d);//ускорение последней точки
                break;
            case NEUMANN_ZERO:
                double lastV=xIter.next();
                result.add(lastV); //пишем как производную координаты скорость последней точки
                result.add((prevprevX - prevX) / (gap * gap) - enviromentResistance * lastV); //ускорение последней точки
        }   
        return result;
    }

    @Override
    protected void applySleeve() {
        double x = left;
        LinkedList<Double> newChain = new LinkedList<>();
        Iterator<Double> chainIter = chain.iterator();
        for (int i = 0; i < length; i++) {
            double y = chainIter.next();
            double vy = chainIter.next();
            if ((x >= sleeve.getLeft()) && (x <= sleeve.getRight()) && (Math.abs(y - sleeve.getY()) >= sleeve.getHeight())) {
                newChain.add(sleeve.getHeight() * Math.signum(y - sleeve.getY()) + sleeve.getY());
                newChain.add(-sleeve.getK() * (vy - sleeve.getVy()) + sleeve.getVy());
            } else {
                newChain.add(y);
                newChain.add(vy);
            }
            x += gap;
        }
        chain = newChain;
    }

    @Override
    public void acceptVisitor(IVisitor visitor) {
        visitor.visitFlexibleStringFast(this);
    }
}
