/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation.model;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import ru.bel_riose.stringsimulation.IAcceptor;
import ru.bel_riose.stringsimulation.IVisitor;

/**
 * Втулка
 * @author Ivan Gudoshnikov
 */
public abstract class Sleeve implements IAcceptor{

    private double k;
    private double height,left, right;
    protected double y,vy;//сдвиг по вертикали, вертикальная скорость.
    
    public Sleeve(double k, double h, double left, double right) {
        this.k = k;
        this.height=h;
        this.left = left;
        this.right = right;
        if(right<=left)throw new EndLesserThanBeginException();
    }

    /**
     * @return коэффициент упругости 
     */
    public double getK() {
        return k;
    }

    /**
     * @return высота разъема
     */
    public double getHeight() {
        return height;
    }
        
    /**
     * @return  начало
     */
    public double getLeft() {
        return left;
    }

    /**
     * @return конец 
     */
    public double getRight() {
        return right;
    }

    public void setK(double k) {
        this.k = k;
    }

    public void setHeight(double h) {
        this.height = h;
    }

    public void setLeft(double begin) {
        this.left = begin;
    }

    public void setRight(double end) {
        this.right = end;
    }

    public double getY() {
        return y;
    }

    public double getVy() {
        return vy;
    }    
    
    public abstract void update(double t, double dt);
        
    public void draw(ShapeRenderer shapeRenderer){
        shapeRenderer.setColor(1, 0.01f, 0.01f, 1);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        //верх
        shapeRenderer.line((float)left, (float)(y+height+(right-left)/10d), (float)left, (float)(y+height));
        shapeRenderer.line((float)left, (float)(y+height), (float)right, (float)(y+height));
        shapeRenderer.line((float)right, (float)(y+height), (float)right, (float)(y+height+(right-left)/10d));
        //низ
        shapeRenderer.line((float)left, (float)(y-(height+(right-left)/10d)), (float)left, (float)(y-height));
        shapeRenderer.line((float)left, (float)(y-height), (float)right, (float)(y-height));
        shapeRenderer.line((float)right, (float)(y-height), (float)right, (float)(y-(height+(right-left)/10d)));
        shapeRenderer.end();
    }

    @Override
    public void acceptVisitor(IVisitor visitor) {
        visitor.visitSleeve(this);
    }
}
