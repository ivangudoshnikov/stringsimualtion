/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation.model;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import java.util.LinkedList;
import ru.bel_riose.stringsimulation.IAcceptor;

class EndLesserThanBeginException extends RuntimeException {
}

class InitialDataMismatchException extends RuntimeException {
}

class NotEnoughElementsException extends RuntimeException {
}

/**
 *
 * @author Ivan Gudoshnikov
 */
public abstract class FlexibleString implements IAcceptor{
    public static enum BoundaryCondition{DIRICHLET_ZERO, NEUMANN_ZERO}
    
    protected double left, right, gap, enviromentResistance;
    protected BoundaryCondition leftCondition, rightCondition;
    protected Sleeve sleeve;

    public FlexibleString(LinkedList<Double> initialHeights, LinkedList<Double> initialSpeeds,double left, double right,BoundaryCondition leftCondition, BoundaryCondition rightCondition, double enviromentResistance,Sleeve sleeve) {
        this.left = left;
        this.right = right;
        this.leftCondition=leftCondition;
        this.rightCondition=rightCondition;
        this.enviromentResistance=enviromentResistance;
        if (right <= left) {
            throw new EndLesserThanBeginException();
        }
        if (initialHeights.size() != initialSpeeds.size()) {
            throw new InitialDataMismatchException();
        }
        if (initialHeights.size() <= 2) {
            throw new NotEnoughElementsException();
        }
        gap = (this.right - this.left) / ((double) initialHeights.size() - 1);
        
        this.sleeve=sleeve;
    }
    
    public abstract void update(double t,double dt);
    public abstract void draw(ShapeRenderer shapeRenderer);

    public double getEnviromentResistance() {
        return enviromentResistance;
    }

    public void setEnviromentResistance(double enviromentResistance) {
        this.enviromentResistance = enviromentResistance;
    }
    
    
    
    protected abstract void applySleeve();
    
}
