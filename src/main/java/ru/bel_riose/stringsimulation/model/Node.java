/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation.model;

import ru.bel_riose.numericalmethods.kinematics2d.Point;
import ru.bel_riose.geometry.Vector;

/**
 *
 * @author Ivan Gudoshnikov
 */
public class Node implements Point {

    private Vector pos, speed;

    public Node(Vector pos, Vector speed) {
        this.pos = pos;
        this.speed = speed;
    }

    @Override
    public Vector getPos() {
        return pos;
    }

    @Override
    public Vector getSpeed() {
        return speed;
    }

    @Override
    public void setPos(Vector v) {
        pos = v;
    }

    @Override
    public void setSpeed(Vector v) {
        speed = v;
    }
}
