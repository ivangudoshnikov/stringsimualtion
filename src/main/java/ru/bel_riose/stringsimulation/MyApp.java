/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation;

import ru.bel_riose.stringsimulation.view.sim_screen.SimScreen;
import com.badlogic.gdx.Game;
import ru.bel_riose.stringsimulation.model.Simulation;

/**
 *
 * @author Ivan
 */
public class MyApp extends Game {

    private Simulation sim;
    private boolean simRunning;

    private int updatesPerFrame = 20;

    public static final String TITLE = "Струна и втулка", VERSION = "v. 0.2.3";

    private SimScreen simScreen;

    private Shotmaker shotmaker;
    private Shotmaker.IRenderCallback shotmakerRender = new Shotmaker.IRenderCallback() {

        @Override
        public void render() {
            MyApp.super.render();
        }
    };

    public Shotmaker getShotmaker() {
        return shotmaker;
    }

    @Override
    public void create() {
        sim = new Simulation();
        simRunning = false;
        shotmaker = new Shotmaker();

        simScreen = new SimScreen(this);
        setScreen(simScreen);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void render() {
        if (isSimRunning()) {
            for (int i = 0; i < updatesPerFrame; i++) {
                shotmaker.doFilming(sim.getT(),shotmakerRender);
                sim.update();
            }

        }
        super.render();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void dispose() {
        super.dispose();
        shotmaker.dispose();
    }

    public boolean isSimRunning() {
        return simRunning;
    }

    public void setSimRunning(boolean simRunning) {
        this.simRunning = simRunning;
    }

    public Simulation getSim() {
        return sim;
    }

    public void restart() {
        sim = new Simulation();
        simRunning = false;
        shotmaker.stopFilming();
    }

    public int getUpdatesPerFrame() {
        return updatesPerFrame;
    }

    public void setUpdatesPerFrame(int updatesPerFrame) {
        this.updatesPerFrame = updatesPerFrame;
    }
}
