/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.stringsimulation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import ru.bel_riose.libgdx_2d_skeleton.ScreenshotFactory;

/**
 *
 * @author Ivan
 */
public class Shotmaker {
    
    public interface IRenderCallback{
        void render();
    }

    public static class ScreenRectangleRegion {

        public final double x, y, w, h;

        public ScreenRectangleRegion(double x, double y, double w, double h) {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
        }
    }

    private ScreenRectangleRegion screenshotRect;
    private boolean filming = false;
    private double lastShotT;
    private double shotDT;
    private final Sound shotSound=Gdx.audio.newSound(Gdx.files.classpath("sfx/camera-shutter-click-01.mp3"));

    public Shotmaker() {
        screenshotRect = new ScreenRectangleRegion(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        shotDT = 2 * 200d;
    }

    public void singleShot() {
        ScreenshotFactory.saveScreenshot((int)screenshotRect.x, (int)screenshotRect.y,
                (int)screenshotRect.w, (int)screenshotRect.h);
        shotSound.play();
    }

    public boolean isFilming() {
        return filming;
    }

    public void stopFilming() {
        filming = false;
    }

    public void startFilming(double t) {
        lastShotT = t - shotDT;
        filming = true;
    }

    public void setScreenshotRect(ScreenRectangleRegion screenshotRect) {
        this.screenshotRect = screenshotRect;
    }

    public void doFilming(double t,IRenderCallback render) {
        if (filming && (lastShotT + shotDT <= t)) {
            lastShotT = t;
            render.render();
            ScreenshotFactory.saveScreenshot(String.valueOf(t) + ".png", (int)screenshotRect.x, (int)screenshotRect.y, (int)screenshotRect.w, (int)screenshotRect.h);
        }
    }

    public double getShotDT() {
        return shotDT;
    }

    public void setShotDT(double shotDT) {
        this.shotDT = shotDT;
    }

    public double getLastShotT() {
        return lastShotT;
    }
    
    public void dispose(){
        shotSound.dispose();
    }
}
