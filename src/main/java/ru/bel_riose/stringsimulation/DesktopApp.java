package ru.bel_riose.stringsimulation;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import java.awt.Dimension;
import java.awt.Toolkit;

/**
 * Hello world!
 *
 */
public class DesktopApp 
{
    public static void main( String[] args )
    {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.useGL20 = true;
                config.fullscreen=true;

                Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                config.vSyncEnabled=true;
                config.width=screenSize.width;
                config.height=screenSize.height;
                config.title=MyApp.TITLE+" "+MyApp.VERSION;
                new LwjglApplication(new MyApp(), config);
    }
}
